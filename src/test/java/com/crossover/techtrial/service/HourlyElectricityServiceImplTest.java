package com.crossover.techtrial.service;

import com.crossover.techtrial.dto.DailyElectricity;
import com.crossover.techtrial.model.HourlyElectricity;
import com.crossover.techtrial.model.Panel;
import com.crossover.techtrial.repository.HourlyElectricityRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class HourlyElectricityServiceImplTest {

    @TestConfiguration
    static public class HourlyElectricityServiceImplTestContextConfiguration {
        @Bean
        public HourlyElectricityService hourlyElectricityService() {
            return new HourlyElectricityServiceImpl();
        }
    }

    @Autowired
    private HourlyElectricityService hourlyElectricityService;

    @MockBean
    private HourlyElectricityRepository hourlyElectricityRepository;

    @MockBean
    Pageable pageable;


    Page pageFound;

    @Before
    public void setUp() {
        Long panelId = 233333L;
        Panel panel = new Panel();
        panel.setId(panelId);
        panel.setSerial("panel_Id12345678");

        HourlyElectricity hourlyElectricity_1 = new HourlyElectricity();
        hourlyElectricity_1.setId(32233322L);
        hourlyElectricity_1.setPanel(panel);

        HourlyElectricity hourlyElectricity_2 = new HourlyElectricity();
        hourlyElectricity_2.setId(32233324L);
        hourlyElectricity_2.setPanel(panel);

        ArrayList<HourlyElectricity> list = new ArrayList<>();
        list.add(hourlyElectricity_1);
        list.add(hourlyElectricity_2);

        pageFound = new PageImpl<HourlyElectricity>(list);
    }

    void getPage(Long id, Pageable pageable){
        Mockito.when(hourlyElectricityRepository.findAllByPanelIdOrderByReadingAtDesc(id, pageable))
                .thenReturn(pageFound);
    }

    @Test
    public void whenFindByPanelId_thenReturnHourlyElectricityPageOrderByReading(){
        getPage(233333L,pageable);
        Page page = hourlyElectricityService.getAllHourlyElectricityByPanelId(233333L,pageable);
        assertEquals(page,pageFound);
        getPage(null,pageable);
        page = hourlyElectricityService.getAllHourlyElectricityByPanelId(null,pageable);
        assertEquals(page,pageFound);
        getPage(null,null);
        page = hourlyElectricityService.getAllHourlyElectricityByPanelId(null,null);
        assertEquals(page,pageFound);
        getPage(233333L,null);
        page = hourlyElectricityService.getAllHourlyElectricityByPanelId(233333L,null);
        assertEquals(page,pageFound);
    }

    @Test
    public void whenFindByIncorrectPanelId_thenReturnHourlyElectricityPageOrderByReadingNotFound(){
        Page page = hourlyElectricityService.getAllHourlyElectricityByPanelId(233334L,pageable);
        assertNull(page);
    }

    @Test
    public void whenFindByNullPanelId_thenReturnHourlyElectricityPageOrderByReadingNotFound(){
        Page page = hourlyElectricityService.getAllHourlyElectricityByPanelId(0L,null);
        assertNull(page);
    }

    @Test
    public void when_Save_thenReturnHourlyElectricity() {
        HourlyElectricity hourlyElectricity = new HourlyElectricity();
        LocalDateTime dateTime = LocalDateTime.now();
        hourlyElectricity.setReadingAt(dateTime);
        hourlyElectricity.setGeneratedElectricity(23L);
        Mockito.when(hourlyElectricityRepository.save(hourlyElectricity)).thenReturn(hourlyElectricity);
        HourlyElectricity response = hourlyElectricityService.save(hourlyElectricity);
        assertTrue(response.equals(hourlyElectricity));
    }

    @Test
    public void when_findAllDailyElectricityStatistics_returnList(){
        DailyElectricity dailyElectricity = new DailyElectricity(LocalDateTime.now(),300L,
                20.22,
                20L,
                40L);
        List dailyElectricityList = new ArrayList();
        dailyElectricityList.add(dailyElectricity);
        Mockito.when(hourlyElectricityRepository.findAllDailyElectricityStatistics(123L)).thenReturn(dailyElectricityList);
        List response = hourlyElectricityService.findAllDailyElectricityStatistics(123L);
        assertEquals(response.get(0), dailyElectricityList.get(0));
    }
}
